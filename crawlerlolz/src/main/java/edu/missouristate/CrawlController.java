package edu.missouristate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CrawlController {

	@GetMapping(value="/login")
	public String getLogin(Model model) {
		return "login.html";
	}
	
	@PostMapping(value="/login")
	public String postLogin(HttpServletRequest req, Model model, 
			String username, String password, String token) {
		
		if ("admin".equalsIgnoreCase(username) &&
				"password".equalsIgnoreCase(password) &&
				"SDVFGlj2132r".equalsIgnoreCase(token)) {
			req.getSession().setAttribute("isLoggedIn", "true");
			return "redirect:/dashboard";	
		}
		
		return "redirect:/login";
	}
	
	@GetMapping(value="/dashboard") 
	public String getDashboard(HttpServletRequest req){
		String isLoggedIn = (String) req.getSession().getAttribute("isLoggedIn");
		
		if ("true".equals(isLoggedIn)) {
			return "dashboard.html";	
		} else {
			return "redirect:/login";
		}
	}
	
	@GetMapping(value="/logout")
	public String getLogout(HttpSession session, Model model) {
		session.removeAttribute("isLoggedIn");
		return "redirect:/login";
	}
}
