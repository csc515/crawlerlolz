package edu.missouristate;

import java.util.HashMap;
import java.util.Map;

import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class RobotChicken {

	private static final String WWW_GOOGLE_COM = "http://www.google.com/";
	private final static String getLoginUrl = "http://localhost:8080/chicken"; 
	private final static String postLoginUrl = "http://localhost:8080/chicken";
	private Map<String, String> data = new HashMap<String, String>();
	private Map<String, String> cookies = new HashMap<String, String>();
	private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36";
	
	public RobotChicken() throws Exception {
		// Visit the site
		// Get/Store Cookies
		// Print Response
		Document doc = visitSite(getLoginUrl);
		printResponse(doc);
		
		// Get the token from the page
		String token = getTokenFromResponse(doc);
		
		// Get Data
		data = getLoginData(token, "ChickenBoss", "KFC123");
		
		// Post the data/cookies to the post login url
		// Print Response (should see the dollar menu)
		doc = postSite(postLoginUrl, data);
		printResponse(doc);
	}

	private Document postSite(String url, Map<String, String> data) throws Exception {
		Response response = Jsoup.connect(url)
				.userAgent(USER_AGENT)
				.referrer(WWW_GOOGLE_COM)
				.cookies(cookies)
				.data(data)
				.header("Accept-Encoding", "gzip, deflate, br")
				
//				Accept-Encoding: gzip, deflate, br
//				Accept-Language: en-US,en;q=0.9
//				Cache-Control: no-cache
//				Connection: keep-alive
//				Content-Length: 137
//				Content-Type: application/x-www-form-urlencoded
				
				.method(Method.POST)
				.followRedirects(true)
				.execute();
		
		return Jsoup.parse(response.body());
	}

	private Map<String, String> getLoginData(String token, String username, 
			String password) {
		
		Map<String, String> data = new HashMap<String, String>();
		data.put("token", token);
		data.put("username", username);
		data.put("password", password);		
		return data;
	}

	private String getTokenFromResponse(Document doc) {
		String token = doc.getElementById("token").attr("value");
		return token;
	}

	private void printResponse(Document doc) {
		System.out.println(doc.html());		
	}

	private Document visitSite(String url) throws Exception {
		Response response = Jsoup.connect(url)
				.userAgent(USER_AGENT)
				.referrer(WWW_GOOGLE_COM)
				.cookies(cookies)
				.method(Method.GET)
				.followRedirects(true)
				.execute();
		
		return Jsoup.parse(response.body());
	}

	public static void main(String[] args) throws Exception {
		new RobotChicken();
	}

}
