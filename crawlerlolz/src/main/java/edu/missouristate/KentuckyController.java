package edu.missouristate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class KentuckyController {

	@GetMapping(value="/chicken")
	public String getChicken() {
		return "chicken.html";
	}
	
	@GetMapping(value="/logoutchicken")
	public String getLogout(HttpServletRequest req, HttpSession session) {
		session.removeAttribute("isLoggedIn");
		req.getSession(true);
		return "redirect:/chicken.html";
	}

	@GetMapping(value="/dollarmenu")
	public String getDashboard(HttpSession session) {
		String loggedIn = (String)session.getAttribute("isLoggedIn");
		
		if ("true".equals(loggedIn)) { 
			return "dollarmenu.html";
		} else {
			return "redirect:/chicken";
		}
	}
	
	@PostMapping(value="/chicken")
	public String postChicken(String token, String username, String password,
			HttpSession session) {
		
		// ColonelSanders01 = token
		// KFC123 = password
		// ChickenBoss = username 
		
		if ("ChickenBoss".equals(username) &&
				"KFC123".equals(password) &&
				"ColonelSanders01".equals(token)) {
			session.setAttribute("isLoggedIn", "true");
			return "redirect:/dollarmenu";
		}
		
		return "redirect:/chicken";
	}
	
	
}
