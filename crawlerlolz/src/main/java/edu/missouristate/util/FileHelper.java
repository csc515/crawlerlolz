package edu.missouristate.util;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.RandomAccessFile;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
//import org.apache.commons.io.FileUtils;
//import org.apache.commons.io.FilenameUtils;
import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
//import org.apache.poi.xssf.usermodel.XSSFRow;
//import org.apache.poi.xssf.usermodel.XSSFSheet;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.util.StringUtils;

//import com.itextpdf.text.Document;
//import com.itextpdf.text.Image;
//import com.itextpdf.text.pdf.PdfCopy;
//import com.itextpdf.text.pdf.PdfReader;
//import com.itextpdf.text.pdf.PdfWriter;
//import com.oreillyauto.model.files.FileAttributes;
//import com.oreillyauto.util.Helper;

public class FileHelper {

	public static FileItem getFileItemByFieldName(List<FileItem> fileItems, String fieldName, boolean ignoreCase){
		
		FileItem fileItem = null;
		
		if (fieldName == null || fileItems == null)
			return fileItem;
		
		Iterator<FileItem> itr = fileItems.iterator();
		
		while (itr.hasNext()) {
			FileItem item = itr.next();
			String currentFieldName = item.getFieldName();
			
			if(ignoreCase){
				if (fieldName.equalsIgnoreCase(currentFieldName))
					return item;
			} else {
				if (fieldName.equals(currentFieldName))
					return item;
			}
		} 
		
		return fileItem;		
	}	

	public static Integer getLineCount(File file) {
		
		Integer lineCount = 0;
		
		FileReader fr = null;
		BufferedReader br = null;
		
		try {
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			
			while ((line = br.readLine()) != null)
				lineCount+=1;
			
			return lineCount;
			
		} catch (Exception e) {
			return lineCount;
		} finally {
			try {
				br.close();
			}catch(Exception e){/*do nothing*/}
			
			try {
				fr.close();
			}catch(Exception e){/*do nothing*/}
		}
	}
	
	public static String getContentsFromFile(File file, String lineDelimiter) {
		BufferedReader br = null;
		FileReader fr = null;
		
		try {
			StringBuilder sb = new StringBuilder();
			fr = new FileReader(file);
		    br = new BufferedReader(fr);
		    String line = "";
		    
		    try {
		        while ((line = br.readLine()) != null) {
		        	sb.append(line+lineDelimiter);
		        }
		    } finally {
		        br.close();
		    }
		    
		    return sb.toString();
		    
		} catch (Exception e) {
			return "";
		} finally {
			try {
				br.close();
			} catch (Exception e) {}
			try {
				fr.close();
			} catch (Exception e) {}
		}
	}
	
	public static String getContentsFromFile(File file) {
		BufferedReader br = null;
		FileReader fr = null;
		
		try {
			StringBuilder sb = new StringBuilder();
			fr = new FileReader(file);
		    br = new BufferedReader(fr);
		    String line = "";
		    
		    try {
		        while ((line = br.readLine()) != null) {
		        	sb.append(line+"\n");
		        }
		    } finally {
		        br.close();
		    }
		    
		    return sb.toString();
		    
		} catch (Exception e) {
			return "";
		} finally {
			try {
				br.close();
			} catch (Exception e) {}
			try {
				fr.close();
			} catch (Exception e) {}
		}
	}

	public static boolean deleteFile(File file, boolean throwExceptionOnError) throws Exception {
		
		try {	
			if (file == null){
				if (throwExceptionOnError)
					throw new Exception("Null file reference! Cannot delete a null file.");	
				else
					return false;				
			}
				
			if (!file.exists()) {
				if (throwExceptionOnError)
					throw new FileNotFoundException("Delete File Failed. Could not find file: " + file.getCanonicalPath());
				else 
					return false;
			}
					
			try {
				file.setWritable(true);
			} catch (Exception e) {}	
			
			return file.delete();
			
		} catch (Exception e) {
			if (throwExceptionOnError)
				throw e;
			else 
				return false;
		}
	}
	
//	public static boolean closeObject(Document document) {
//		try {
//			document.close();
//			return true;
//		} catch (Exception e) {
//			return false;
//		}
//	}
	
	public static boolean closeObject(PreparedStatement ps) {
		try {
			ps.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static boolean closeObject(Closeable closeable) {
		
		try {	
			if (closeable == null) {
				return false;
			}	
			
			closeable.close();
			return true;
			
		} catch (Exception e) {
			return false;
		}
	}
	
	public static boolean closeObject(Closeable closeable, boolean throwExceptionOnError) throws Exception {
		
		try {	
			if (closeable == null) {
				if (throwExceptionOnError)
					throw new Exception("Null closeable reference! Cannot close a null reference.");
				else
					return false;
			}
			
			closeable.close();
			return true;
			
		} catch (Exception e) {
			if (throwExceptionOnError)
				throw e;
			else 
				return false;
		}
	}
	
	public static void writeDataToFile(String data, File file) throws Exception {
		
		try (PrintStream out = new PrintStream(new FileOutputStream(file))) {
		    out.print(data);
	    } catch (Exception e) {
	    	e.printStackTrace();
	    	throw new Exception("FileHelper.writeDataToFile() Error! e.getMessage()=" + e.getMessage());
	    }
	}
	
	public static void writeBase64DataToFile(String encodedData, File file) throws Exception {

	    byte[] data = Base64.decodeBase64(encodedData);
	    
	    try (OutputStream stream = new FileOutputStream(file)) {
	        stream.write(data);
	    } catch (Exception e) {
	    	e.printStackTrace();
	    	throw new Exception("FileHelper.writeBase64DataToFile() Error! e.getMessage()=" + e.getMessage());
	    }
	}

	public static void consumeEntity(HttpEntity entity) throws Exception {
		try {
			EntityUtils.consume(entity);
		} catch (Exception e) {
			throw new Exception("FileHelper.consumeEntity: exception ignored. e.getMessage="+e.getMessage());
		}
	}
	
	public static void consumeEntity(HttpEntity entity, boolean throwException) throws Exception {
		try {
			EntityUtils.consume(entity);
		} catch (Exception e) {
			if (throwException)
				throw e;
		}
	}
	
//	public static void uploadFile(FileItem fileItem, String fileName, File directory, HttpServletRequest request) throws Exception {
//
//		File file;
//		String filePath = directory.getAbsolutePath();
//		fileName = (StringUtils.hasLength(fileName)) ? fileName : fileItem.getName();
//		fileName = StringHelper.removeSpecialCharactersAndSpaces(fileName);
//		String separator = System.getProperty("file.separator");
//
//		if (fileName.lastIndexOf(separator) >= 0) {
//			file = new File(filePath, (fileName.substring(fileName.lastIndexOf(separator))));
//		} else {
//			file = new File(filePath, (fileName.substring(fileName.lastIndexOf(separator) + 1)));
//		}
//
//		fileItem.write(file);
//	}
	
	public static byte[] loadFile(File file) throws Exception {
	    
		InputStream is = null;
		
		try {
			is = new FileInputStream(file);
		    long length = file.length();
		    
		    if (length > Integer.MAX_VALUE) {
		        // File is too large
		    }
		    
		    byte[] bytes = new byte[(int)length];
		    
		    int offset = 0;
		    int numRead = 0;
		    
		    while (offset < bytes.length
		           && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
		        offset += numRead;
		    }

		    if (offset < bytes.length) {
		        throw new IOException("Could not completely read file "+file.getName());
		    }

		    is.close();
		    return bytes;			
		} finally {
			closeObject(is, false);
		}
	}

	public static boolean flush(ServletOutputStream out) {
		try {
			out.flush();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean flushAndCloseObject(OutputStream out) {
		try {
			out.flush();
			out.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static boolean flushAndCloseObject(ServletOutputStream out) {
		try {
			out.flush();
			out.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

//	public static void addLogoToPDF(File pdfFile, float xPosition, float yPosition) {
//        Document document = new Document();
//
//        try {
//            PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
//            document.open();
//            String imageUrl = "some-image.png"; // set your image path here!
//            Image image = Image.getInstance(new URL(imageUrl));
//            image.setAbsolutePosition(xPosition, yPosition);
//            document.add(image);
//        } catch(Exception e){
//            e.printStackTrace();
//        } finally {
//        	closeObject(document);
//        }
//	}
	
//	public static String getFilenameExtension(String filename) {
//		
//		if (StringUtils.hasText(filename))
//			return FilenameUtils.getExtension(filename);
//		
//		return null;
//	}

	public static String prepareHtmlForPDF(String html, boolean isDecodedHtml) throws Exception {
		
		if (!StringUtils.hasText(html))
			return html;
		
		if (!isDecodedHtml)
			html = URLDecoder.decode(html, "UTF-8");
		
		if (!StringUtils.hasText(html))
			return html;
		
		if (html.indexOf("gbtspring") > 0)
			html = html.replace("\"/gbtspring/assets/", "\"https://www.gobytruck.com/assets/");	
		else
			html = html.replace("\"/assets/", "\"https://www.gobytruck.com/assets/");
		
		html = html.replace("'//www.google", "'https://www.google"); 
		html = html.replace("'//fonts", "'https://fonts");
		html = html.replace("\"//fonts", "\"https://fonts");
		
		// Replace all &nbsp; with &#160;
		//html = html.replace("&nbsp;", "&#160;"); <-- still breaking.
		html = html.replace("&nbsp;", "");
				
		return html;
	}
	
	public static boolean closeObjectAndCleanJVM(Closeable closeable) {
		
		try {	
			if (closeable == null)
				return true;
			
			closeable.close();
			
			try {
				Helper.gc();
			} catch (Exception e) {}
			
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static boolean isBinaryFile(File f) throws FileNotFoundException, IOException {
	    
		FileInputStream in = new FileInputStream(f);
	    int size = in.available();
	    
	    if (size > 1024) 
	    	size = 1024;
	    
	    byte[] data = new byte[size];
	    in.read(data);
	    in.close();
	    int ascii = 0;
	    int other = 0;

	    for (int i = 0; i < data.length; i++) {
	        byte b = data[i];
	        
	        if ( b < 0x09 ) 
	        	return true;

	        if( b == 0x09 || b == 0x0A || b == 0x0C || b == 0x0D ) 
	        	ascii++;
	        else if ( b >= 0x20  &&  b <= 0x7E ) 
	        	ascii++;
	        else 
	        	other++;
	    }

	    if ( other == 0 ) 
	    	return false;

	    return (100 * other / (ascii + other) > 95);
	}

	public static void prettyPrintFileSize(File file) {
		
		if (file.exists()) {
			double bytes = file.length();
			double kilobytes = (bytes / 1024);
			double megabytes = (kilobytes / 1024);
			double gigabytes = (megabytes / 1024);
			System.out.println("kilobytes : " + kilobytes);
			System.out.println("megabytes : " + megabytes);	
		}
	}

	public static boolean copyFile(File sourceFile, File destinationDirectory) throws Exception {
		
		try {
			if (sourceFile == null || destinationDirectory == null)
				return false;
			
			if (!sourceFile.exists() || !destinationDirectory.exists())
				return false;
	
			String filename = sourceFile.getName();
			
			Path source = Paths.get(sourceFile.getAbsolutePath());
			Path destination = Paths.get(destinationDirectory + System.getProperty("file.separator") + filename);
 
			Files.copy(source, destination);
			return true;
			
		} catch (FileAlreadyExistsException fe) {
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public static File fixFileName(File file, boolean overwriteTarget) throws Exception {
		
		if (file == null || !file.exists())
			return file;
		
		boolean success = true;
		String directory = file.getParent();
		String originalFilename = file.getName();
		String newFilename = originalFilename.replaceAll("[^a-zA-Z0-9.-]", "_");

		String filename = (originalFilename.equals(newFilename)) ? originalFilename : newFilename;  
				
		// File (or directory) with new name
		File file2 = new File(directory, filename);

		if (!originalFilename.equals(newFilename) && file2.exists()) {
			if (overwriteTarget) {
				file2.delete();
				file2 = new File(directory, filename);
			} else {
				throw new java.io.IOException("file exists");
			}
		}

		// Rename file (or directory)
		if (!originalFilename.equals(newFilename)) {
			success = file.renameTo(file2);
			
			if (!success) 
				throw new Exception("Rename file failed.");
		}
		
		return file2;
	}

	public static boolean renameFile(File file, String newFilename, boolean overwriteTarget) throws Exception {
		
		if (file == null || !file.exists())
			return false;
		
		boolean success = false;
		String directory = file.getParent();
		String originalFilename = file.getName();
		String filename = (originalFilename.equals(newFilename)) ? originalFilename : newFilename;  
		File file2 = new File(directory, filename);

		if (!originalFilename.equals(newFilename) && file2.exists()) {
			if (overwriteTarget) {
				file2.delete();
				file2 = new File(directory, filename);
			} else {
				throw new java.io.IOException("file exists");
			}
		}

		// Rename file (or directory)
		if (!originalFilename.equals(newFilename)) {
			success = file.renameTo(file2);
			
			if (!success) 
				throw new Exception("Rename file failed.");
		}
		
		return success;
	}

	public static boolean renameDirectory(File oldDirectory, File newDirectory, boolean overwriteTarget)
			throws Exception {
		
		// Initialize variables
		boolean success = false;
		
		// Make sure the old directory exists
		if (oldDirectory == null || !oldDirectory.exists())
			return success;
		
		// If the new directory exists and overwriteTarget is false, return
		if (newDirectory.exists() && overwriteTarget == false)
			return success;
		
		// Rename the old directory to the new directory name
		success = oldDirectory.renameTo(newDirectory);
		
		return success;
	}
	
	public static boolean renameFile(File file, File renamedFile, boolean overwriteTarget) throws Exception {
		
		if (file == null || !file.exists())
			return false;
		
		boolean success = false;

		if (renamedFile.exists()) {
			if (overwriteTarget) {
				renamedFile.delete();
			} else {
				throw new java.io.IOException("file exists");
			}
		}

		// Rename file (or directory)
		success = file.renameTo(renamedFile);
			
		if (!success) 
			throw new Exception("Rename file failed.");
		
		return success;
	}
	
	public boolean isFileWriteComplete(File file) {
		
		if (file == null || !file.exists())
			return true;
		
	    RandomAccessFile stream = null;
	    
	    try {
	        stream = new RandomAccessFile(file, "rw");
	        return true;
	    } catch (Exception e) {
	        return false;
	    } finally {
	        if (stream != null) {
	            try {
	                stream.close();
	            } catch (IOException e) {}
	        }
	    }
	}

//	public static FileAttributes getFileAttributes(File file) throws IOException {
//
//		FileAttributes fileAttributes = null;
//		
//		if (file == null || !file.exists())
//			return fileAttributes;
//		
//		Path path = file.toPath();
//		BasicFileAttributes attr = Files.readAttributes(path, BasicFileAttributes.class);
//		fileAttributes = new FileAttributes();
//		
//		Timestamp creationTime = new Timestamp(attr.creationTime().toMillis());
//		Timestamp lastAccessTime = new Timestamp(attr.lastAccessTime().toMillis());
//		Timestamp lastModifiedTime = new Timestamp(attr.lastModifiedTime().toMillis());
//		
//		fileAttributes.setCreationTime(creationTime);
//		fileAttributes.setLastAccessTime(lastAccessTime);
//		fileAttributes.setLastModifiedTime(lastModifiedTime);
//		fileAttributes.setSize(attr.size());
//		
//		return fileAttributes;
//	}
	
//	public static boolean moveFile(File srcFile, File destFile) {
//		try {
//			FileUtils.moveFile(srcFile, destFile);
//			return true;
//		} catch (Exception e) {
//			return false;
//		}
//	}

//	public static String getFileItemExtension(FileItem item) {		
//		try {
//			return getFilenameExtension(item.getName());
//		} catch (Exception e) {
//			return null;
//		}
//	}

	public static HttpServletResponse addFileToResponse(HttpServletResponse response, File file, String contentType) throws Exception {
		String mimeType = new MimetypesFileTypeMap().getContentType(file);
		response.setContentType(mimeType);
		InputStream is = new FileInputStream(file);
		
		if (StringUtils.hasText(contentType))
			response.addHeader("Content-type", contentType);
		
		response.addHeader("Content-Disposition", "attachment; filename="+ file.getName().replace(" ", "_"));
		response.setContentLength((int) file.length());
		IOUtils.copy(is, response.getOutputStream());
		response.flushBuffer();
		return response;
	}

//	public static boolean isImageFile(File file) {
//
//		if (file == null)
//			return false;
//
//		String mimetype = new MimetypesFileTypeMap().getContentType(file);
//		String type = mimetype.split("/")[0];
//
//		if ("image".equalsIgnoreCase(type)) {
//			return true;
//		} else {
//			String ext = FileHelper.getFilenameExtension(file.getName());
//			ext = (ext == null) ? "" : ext.toLowerCase();
//			
//			if ("gif".equals(ext) || "jpeg".equals(ext) || "jpg".equals(ext) || "png".equals(ext)
//					|| "bmp".equals(ext) || "tiff".equals(ext)) {
//				return true;
//			}
//		}
//
//		return false;
//	}
	
	public static boolean isPdfFile(File file) {

		Scanner input = null;
				
		if (file == null)
			return false;
				
		try {
			input = new Scanner(new FileReader(file));
			
			while (input.hasNextLine()) {
				final String checkline = input.nextLine();
				
				if (checkline.contains("%PDF-"))
					return true;
				else if (file.getName().toLowerCase().endsWith(".pdf"))
					return true;
			}
		} catch (FileNotFoundException fnfe) {
			return false;
		} finally {
			try {
				input.close();
			} catch (Exception e) {}
		}

		return false;
	}
	
	// This method takes a reference to a folder and creates it if it doesn't exist and can empty it if it was 
	// already created. ~jlb 5/1/2017
//	public static boolean createFolder(File folder, boolean buildFolderStructure, boolean emptyFolder)
//			throws Exception {
//
//		boolean created = false;
//		
//		if (folder == null || folder.isFile())
//			return created;
//		
//		// Create folder or clean the target directory
//		if (folder.exists() && emptyFolder) {
//			FileUtils.cleanDirectory(folder);
//			created = true;
//		} else {
//			if (buildFolderStructure)
//				created = folder.mkdirs();
//			else
//				created = folder.mkdir();
//		}
//		
//		return created;
//	}
	
	// This method takes any image and converts it to a PDF. ~jlb 5/1/2017
//	public static boolean convertImageToPDF(File imageFile, File targetFolder, String targetFilename) {
//		
//		Document document = new Document();
//
//		try {
//			File targetFile = new File(targetFolder, targetFilename);
//			PdfWriter.getInstance(document, new FileOutputStream(targetFile));
//			document.open();
//			
//			// Chapter Indentation
//			int indentation = 0;
//			
//			Image image1 = Image.getInstance(imageFile.getAbsolutePath());
//			
//			// Calculate Scaler
//			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
//		               - document.rightMargin() - indentation) / image1.getWidth()) * 100;
//			
//			// Set scale percentage
//			image1.scalePercent(scaler);
//			
//			// Add the image to the iText doc and close
//			document.add(image1);
//			document.close();
//			return true;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return false;
//		}
//	}

	// This method takes any image from a web address (URL) and converts it to a PDF. ~jlb 5/1/2017
//	public static boolean convertImageUrlToPDF(String imageUrl, File targetFolder, String targetFilename) {
//		
//		Document document = new Document();
//
//		try {
//			File targetFile = new File(targetFolder, targetFilename);
//			PdfWriter.getInstance(document, new FileOutputStream(targetFile));
//			document.open();
//			Image image = Image.getInstance(new URL(imageUrl));
//			document.add(image);
//			document.close();
//			return true;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return false;
//		}
//	}

//	public static File convertCsvToXlsx(File csvFile) throws Exception {
//		
//		if (csvFile != null && csvFile.exists()) {
//			BufferedReader br = null;
//			XSSFWorkbook workBook = null;
//			
//			try {
//				File directory = new File(csvFile.getParent());
//				String csvFilename = csvFile.getName();
//
//				if (csvFilename.indexOf(".") != -1)
//					csvFilename = csvFilename.substring(0, csvFilename.indexOf("."));
//
//				File xlsxFile = new File(directory, (csvFilename + ".xlsx"));
//
//				workBook = new XSSFWorkbook();
//				XSSFSheet sheet = workBook.createSheet("Sheet1");
//				String currentLine = null;
//				int RowNum = 0;
//				br = new BufferedReader(new FileReader(csvFile));
//
//				while ((currentLine = br.readLine()) != null) {
//					String str[] = currentLine.split(",");
//					RowNum++;
//					XSSFRow currentRow = sheet.createRow(RowNum);
//
//					for (int i = 0; i < str.length; i++)
//						currentRow.createCell(i).setCellValue(str[i]);
//				}
//
//				FileOutputStream fileOutputStream = new FileOutputStream(xlsxFile);
//				workBook.write(fileOutputStream);
//				fileOutputStream.close();	
//			} finally {				
//				try {
//					br.close();
//				} catch (Exception e) {}
//			}
//		}
//		
//		return null;
//	}
	
}
