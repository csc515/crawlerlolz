package edu.missouristate.util;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.util.StringUtils;

public class CrawlerHelper {

	private static int debug = 1;

	public static RequestResponse postPage(CloseableHttpClient httpclient, String url, boolean consumeEntity, 
			RequestResponse requestResponse, BasicCookieStore cookieStore) throws Exception {
		
		if (debug > 0) System.out.println("POST : page=" + url);
		requestResponse = (requestResponse == null) ? new RequestResponse() : requestResponse;
		RequestBuilder requestBuilder = RequestBuilder.post().setUri(new URI(url));
		Map<String, String> paramMap = requestResponse.getParameterMap();
		Map<String, String> headerMap = requestResponse.getHeaderMap();
		
		for (Map.Entry<String, String> entry : paramMap.entrySet()) {
		    String key = entry.getKey();
		    String value = entry.getValue();
		    requestBuilder.addParameter(key, value);
		}

		for (Map.Entry<String, String> entry : headerMap.entrySet()) {
		    String key = entry.getKey();
		    String value = entry.getValue();
		    requestBuilder.addHeader(key, value);
		}
		
		HttpUriRequest postRequest = requestBuilder.build();
		CloseableHttpResponse response = httpclient.execute(postRequest);
		RequestResponse postRequestResponse = null;
		HttpEntity entity = response.getEntity();

		try {
			postRequestResponse = processResponse(response, entity, requestResponse, cookieStore);
		} finally {
			if (consumeEntity)
				FileHelper.consumeEntity(entity);

			FileHelper.closeObject(response);
		}
		
		return postRequestResponse;
	}

	public static RequestResponse postJsonPage(RequestResponse requestResponse, boolean consumeEntity, 
			String url, String json, BasicCookieStore cookieStore, boolean throwConsumeEntityError) throws Exception {
		
		requestResponse = (requestResponse == null) ? new RequestResponse() : requestResponse;
		
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(url);
		 
		StringEntity stringEntity = new StringEntity(json);
		httpPost.setEntity(stringEntity);
		//httpPost.setHeader("Accept", "application/json");
		httpPost.setHeader("Content-type", "application/json");
		 
	    CloseableHttpResponse response = client.execute(httpPost);
	    client.close();

	    RequestResponse postRequestResponse = null;
		HttpEntity entity = response.getEntity();

		try {
			postRequestResponse = processResponse(response, entity, requestResponse, cookieStore);
		} finally {
			if (consumeEntity) {
				try {
					FileHelper.consumeEntity(entity);	
				} catch(Exception e) {
					if (throwConsumeEntityError) {
						throw e;
					} else {
						System.out.println("Error ignored: " + e.getMessage());
					}
				}
			}

			FileHelper.closeObject(response);
		}
		
		return postRequestResponse;	    
	}
	
	public static void printResponseBody(String responseBody, boolean keepCarriageReturns) {
		if (StringUtils.hasText(responseBody)) {
			if (!keepCarriageReturns)
				responseBody = responseBody.replace("\r\n", "");

			if (debug > 0) System.out.println(responseBody);
		}
	}

	public static RequestResponse getPage(CloseableHttpClient httpclient, String url, boolean consumeEntity, 
			RequestResponse requestResponse, BasicCookieStore cookieStore) throws Exception {

		if (debug > 0) System.out.println("GET : page=" + url);
		HttpGet httpget = new HttpGet(url);
		CloseableHttpResponse response = httpclient.execute(httpget);
		HttpEntity entity = response.getEntity();
		requestResponse = (requestResponse == null) ? new RequestResponse() : requestResponse;
		
		try {
			requestResponse = processResponse(response, entity, requestResponse, cookieStore);
		} finally {
			if (consumeEntity)
				FileHelper.consumeEntity(entity);

			FileHelper.closeObject(response);
		}

		return requestResponse;
	}

	public static RequestResponse processResponse(CloseableHttpResponse response, HttpEntity entity, 
			RequestResponse requestResponse, BasicCookieStore cookieStore) throws Exception {

		if (debug > 0) System.out.println("Page Response Code: " + response.getStatusLine());
		String tempResponseBody = requestResponse.getResponseBody();
		String responseBody = "";
		
		// Get the idSearchList from the requestResponse before clearing it
		Map<String, String> idSearchMap = requestResponse.getIdSearchMap();
		
		// Build a new requestResponseData object and process the response/entity
		requestResponse = new RequestResponse();
		
		try {
			responseBody = EntityUtils.toString(entity);	 
		} catch(Exception e) {
			responseBody = tempResponseBody;
		}
		
		// Find all hidden elements. These will be needed if we post to this page later
		Document doc = Jsoup.parse(responseBody);
		Elements elements = doc.select("input[type=hidden]");
		
		for (Element element : elements) {
			String id = element.id();
			String value = element.val();
			requestResponse.addHiddenElement(id, value);
		}
		
		// Find all values from the ID Search List and place them in the ID Search Map
		for (Map.Entry<String, String> entry : idSearchMap.entrySet()) {
			String id = entry.getKey();
			String type = entry.getValue();
			Element element = doc.getElementById(id);
			
			switch (type) {
			case RequestResponse.TEXT:
				requestResponse.addIdSearchValue(id, (element == null) ? null : element.text());
				break;
			case RequestResponse.VALUE:
				requestResponse.addIdSearchValue(id, (element == null) ? null : element.val());
				break;
			default:
				requestResponse.addIdSearchValue(id, (element == null) ? null : element.val());
				break;
			}
		}
		
		requestResponse.setEntity(entity);
		requestResponse.setResponse(response);
		requestResponse.setResponseBody(responseBody);
		printCookies(cookieStore);
		printResponseBody(responseBody, false);
		return requestResponse;
	}

	public static void printCookies(BasicCookieStore cookieStore) {
		
		if (cookieStore == null || cookieStore.getCookies() == null || cookieStore.getCookies().size() == 0)
			return;
			
		List<Cookie> cookies = cookieStore.getCookies();

		if (cookies.isEmpty()) {
			if (debug > 0) System.out.println("  No cookies");
		} else {
			for (int i = 0; i < cookies.size(); i++) {
				Cookie cookie = cookies.get(i);
				String name = cookie.getName();
				String value = cookie.getValue();
				if (debug > 0) System.out.println("  <cookie> " + name + "=" + value);
			}
		}
	}
}
