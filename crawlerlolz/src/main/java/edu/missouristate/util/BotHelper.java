package edu.missouristate.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class BotHelper {

	public static Response postWalmartLogin(Integer timeout, Map<String, String> cookies,
			String login, String password) throws Exception {
		
		cookies = (cookies == null) ? new HashMap<String, String>() : cookies;
		timeout = (timeout == null) ? timeout : timeout;
		return Jsoup.connect(Settings.USER_LOGIN_POST_PAGE)
				.timeout(timeout)
				.ignoreContentType(true)
				.method(Connection.Method.POST)
				.cookies(cookies)
				.header("authority", "www.walmart.com")
				.header("method", "POST")
				.header("path", "/account/electrode/api/signin?ref=domain")
				.header("scheme", "https")
				.header("accept", "*/*")
				.header("accept-encoding", "gzip, deflate, br")
				.header("accept-language", "en-US,en;q=0.9")
				.header("cache-control", "no-cache")
				.header("content-length", "3770")
				.header("content-type", "application/json")
				.header("user-agent", Settings.USERAGENT)
				.data("username", login)
				.data("password", password)
				.data("rememberme", "true")
				.data("showRememberme", "true")
				.requestBody(getWamartLoginJson(login, password))
				.execute();
	}

	public static String getWamartLoginJson(String login, String password) {
		String json = "{\"username\":\""+login+"\",\"password\":\""+password+"\",\"rememberme\":true,\"showRememberme\":\"true\",\"captcha\":{\"sensorData\":\"2a25G2m84Vrp0o9c4151861.12-1,8,-36,-890,Mozilla/9.8 (Windows NT 52.1; Win95; x80) AppleWebKit/791.17 (KHTML, like Gecko) Chrome/28.1.3210.282 Safari/434.67,uaend,93837,23139723,en-US,Gecko,9,6,2,1,291975,4143204,8433,144,7790,671,585,830,3178,,cpen:3,i0:2,dm:1,cwen:1,non:5,opc:0,fc:0,sc:4,wrc:1,isc:9,vib:9,bat:1,x10:2,x30:8,8028,0.352067565338,901193181010.2,loc:-3,6,-01,-804,do_en,dm_en,t_en-0,4,-12,-002,3,2,6,7,701,427,3;1,8,6,3,952,586,0;6,6,2,2,0347,1,9;2,-3,1,9,-1,-1,6;7,-9,7,0,-4,-0,2;4,0,7,1,1501,553,2;1,-1,0,2,0791,002,9;7,5,0,7,339,699,7;4,2,6,7,3977,853,1;9,2,4,9,8551,3,0;6,8,2,2,676,830,6;6,-9,7,0,-4,-0,2;5,-2,9,7,-2,-7,6;2,3,9,8,992,476,2;2,1,7,4,1016,201,7;0,0,1,0,3280,1,9;7,-2,9,2,-3,-8,0;1,-1,6,6,-9,-0,7;3,2,6,7,3246,907,1;9,-7,2,2,0393,920,2;4,0,7,1,976,482,4;9,9,0,1,2394,425,1;9,7,3,1,7066,8,7;0,2,1,0,3856,699,7;4,2,6,7,3384,853,1;9,2,4,9,9978,3,0;7,8,2,2,0486,982,2;4,8,7,1,1220,6,6;2,3,9,8,097,476,2;1,1,7,4,1525,5336,9;7,-2,9,2,6666,427,3;1,8,6,2,3716,814,9;2,4,8,8,2223,0,6;-5,0,-84,-412,6,8,3,2,427,830,6;7,4,2,0,586,879,6;2,1,9,8,4476,2,4;8,-0,7,3,-0,-7,2;2,-1,0,1,-7,-5,8;7,2,0,2,0817,118,9;7,-2,9,3,5356,728,3;0,8,7,3,866,223,0;7,8,3,2,0130,982,2;4,8,7,1,1964,6,6;2,3,9,8,089,146,2;1,-1,0,1,-7,-5,8;8,-8,3,0,-3,-3,1;9,9,3,1,208,031,9;8,5,0,7,7572,573,0;1,9,2,5,9717,7,3;0,-3,4,8,-0,-1,1;0,-7,2,1,-1,-4,0;6,8,2,2,0409,036,2;4,-2,9,8,4422,085,8;7,2,0,2,807,948,7;1,2,1,0,3940,952,7;3,0,6,7,3619,0,0;1,1,3,5,9383,223,0;7,8,3,2,0547,982,2;4,8,7,1,2381,6,6;3,3,9,8,4515,047,8;7,0,0,2,0536,2,1;9,9,3,1,303,031,9;7,5,0,7,7081,8052,3;0,-3,4,8,9382,830,6;7,4,1,9,9057,745,4;8,7,0,1,3516,6,2;-8,-8,4,0,-3,623,7;-8,-2,0,2,-3,388,7;-2,-7,7,2,-8,278,1;-7,-5,9,7,-8,532,6;-5,-2,0,7,-2,169,4;-2,-1,1,1,-7,873,1;-1,-4,1,6,-5,9951,7;-2,-7,7,2,-8,1175,0;-3,-3,2,9,-1,2646,6;-9,-0,8,3,-0,3269,1;-1,-4,1,6,-5,9528,7;-2,-7,7,2,-8,204,1;-7,-5,9,7,-8,529,6;-5,-2,0,7,-2,777,4;-2,-1,1,1,-7,750,1;-1,-4,1,6,-5,921,0;-4,-0,3,4,-2,107,3;-0,-7,3,1,-1,142,9;-7,-9,8,0,-4,308,6;-9,-0,8,3,-0,485,8;-0,-1,2,9,-7,502,9;-1,-1,7,6,-9,906,0;-1,-3,5,8,-0,928,0;-3,-3,2,9,-1,435,2;-3,3,-91,-207,2,5,793,undefined,4,8,-0;8,5,974,undefined,3,0,-3;6,9,682,undefined,8,7,-8;6,2,518,undefined,0,6,-5;2,8,964,undefined,7,0,-4;5,8,536,undefined,6,6,-9;3,1,925,undefined,0,0,-1;3,8,176,undefined,6,2,-8;8,1,094,undefined,0,1,-7;5,4,064,undefined,2,1,-1;13,1,512,undefined,0,6,-5;92,1,691,undefined,9,7,-2;08,7,88743,-4,1,9,-7;31,9,67755,-0,6,2,-8;-8,5,-80,-538,7,1,373,356,584;8,1,376,319,583;9,1,399,258,585;0,1,309,231,585;1,1,470,299,583;2,1,737,298,588;3,1,745,296,571;4,1,762,292,560;5,1,778,289,554;6,1,795,286,551;89,8,091,940,312;01,3,098,588,948,1085,3;74,8,9293,027,127,7700,0;10,4,34343,433,2;84,2,21990,071,8;18,1,97381,345,84;26,7,99947,200,50;03,7,53852,027,37;34,3,42696,654,14;51,2,25519,359,20;01,0,05142,906,02;90,8,66835,044,285;92,2,21020,223,108;33,7,99063,331,474;86,5,16589,487,050;67,2,25619,466,864;33,1,38032,648,053;367,4,69128,075,438,-1;914,6,81987,578,084,-9;089,2,84943,686,158,-4;-0,4,-12,-014,-2,1,-58,-280,7,70,-7,-5,-2;0,05657,-3,-3,-8;2,59089,-7,-9,-0;-1,3,-56,-380,9,16,-1,-3,-3,-8,-8,-2,-7,-5,-2;0,05644,-3,-3,-8,-8,-2,-7,-5,-2,-1;2,66817,-3,-8,-8,-2,-7,-5,-2,-1,-4;-0,4,-12,-011,-2,1,-58,-272,9,6936;2,7026;0,05575;9,99452;1,13568;6,07444;0,26172;9,10060;1,31611;9,37130;-1,2,-93,-754,-8,2,-25,-731,NaN,646020,9,18797,61308,1,NaN,16941,1308254143641,8502186513047,25,07874,13,664,0930,1,3,73768,046133,0,hd50mj7ds8c89zsjv3nk_7106,9996,465,7021907581,33360219-3,3,-91,-205,-7,2-2,1,-97,-66,7872321262;22,76,57,23,14,6,49,60,9;,4,8;true;true;true;063;true;66;31;true;false;-9-8,2,-25,-42,9089-0,9,-04,-370,254655119-0,4,-12,-015,493740-5,0,-84,-431,;18;4;7\"}}";
		return json;
	}

	public static String getAddToCartJson(String offerId) {
		//String json = "{\"offerId\":\""+offerId+"\",\"quantity\":1,\"location\":{\"postalCode\":\"42101\",\"city\":\"Bowling Green\",\"state\":\"KY\",\"isZipLocated\":true},\"shipMethodDefaultRule\":\"SHIP_RULE_1\",\"storeIds\":[5236,299,736]}";
		String json = "{\"offerId\":\""+offerId+"\",\"quantity\":1,\"location\":{\"postalCode\":\"65809\",\"city\":\"Springfield\",\"state\":\"MO\",\"isZipLocated\":true},\"shipMethodDefaultRule\":\"SHIP_RULE_1\",\"storeIds\":[2221,3238,5693,2839,444]}";
		return json;
	}
	
	public static Response getPage(String page, Map<String, String> cookies, Integer timeout) throws Exception {
		cookies = (cookies == null) ? new HashMap<String, String>() : cookies;
		timeout = (timeout == null) ? Settings.DEFAULT_TIMEOUT : timeout;
		return Jsoup.connect(page)
				.timeout(timeout)
				.method(Connection.Method.GET)
				.cookies(cookies)
				.execute();
	}

	public static boolean isLoggedIntoWalmart(String html) {
		if (html == null || html.length() == 0) {
			System.out.println("Empty HTML! Not Authenticated!");
			return false;
		} else if (html.contains(Settings.LOGIN_LINK)) {
			System.out.println("Not Logged In!!! Authentication Failed!");
			return false;
		} else if (html.contains(Settings.LOGGED_IN_TEXT)) {
			System.out.println("Logged In!!! Authentication Succeeded!");
			return true;
		} else if (html.contains(Settings.LOGIN_PAGE_TEXT)) {
			System.out.println("Not Logged In!!! Authentication Failed!");
			return false;
		} else {
			System.out.println("Can't tell if you're logged in! Authentication Unknown!");
			return false;
		}
	}

	public static void loadBrowser(String url) {
		String os = System.getProperty("os.name").toLowerCase();
		Runtime rt = Runtime.getRuntime();

		try {
			if (os.indexOf("win") >= 0) {
				// this doesn't support showing urls in the form of "page.html#nameLink"
				rt.exec("rundll32 url.dll,FileProtocolHandler " + url);
			} else if (os.indexOf("mac") >= 0) {
				rt.exec("open " + url);
			} else if (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0) {
				// Do a best guess on unix until we get a platform independent way
				// Build a list of browsers to try, in this order.
				String[] browsers = { "epiphany", "firefox", "mozilla", "konqueror", "netscape", "opera", "links",
						"lynx" };

				// Build a command string which looks like "browser1 "url" || browser2 "url"
				// ||..."
				StringBuffer cmd = new StringBuffer();
				for (int i = 0; i < browsers.length; i++)
					cmd.append((i == 0 ? "" : " || ") + browsers[i] + " \"" + url + "\" ");

				rt.exec(new String[] { "sh", "-c", cmd.toString() });
			} else {
				return;
			}
		} catch (Exception e) {
			return;
		}
	}

	public static void writeToFile(String filename, String data) {
		try {
			File file = new File(filename);
			
			if (file.exists()) {
				file.delete();
			}
			
			FileWriter myWriter = new FileWriter(file);
			myWriter.write(data);
			myWriter.close();
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
	}

	public static boolean pageContains(String html, String str) {
		if (html != null && html.length() > 0) {
			return html.contains(str);
		}
		
		return false; 
	}

	public static Response postAddToCart(Integer timeout, Map<String, String> cookies, 
			String offerId) throws Exception {
		
		cookies = (cookies == null) ? new HashMap<String, String>() : cookies;
		timeout = (timeout == null) ? Settings.DEFAULT_TIMEOUT : timeout;
		return Jsoup.connect(Settings.USER_LOGIN_POST_PAGE)
				.timeout(timeout)
				.ignoreContentType(true)
				.method(Connection.Method.POST)
				.cookies(cookies)
				
				.header("authority", "www.walmart.com")
				.header("method", "POST")
				.header("path", "/api/v3/cart/:CRT/items")
				.header("scheme", "https")
				.header("accept", "application/json")
				.header("accept-encoding", "gzip, deflate, br")
				.header("accept-language", "en-US,en;q=0.9")
				.header("cache-control", "no-cache")
				.header("content-length", "213")
				.header("content-type", "application/json")
				.header("user-agent", Settings.USERAGENT)
			
				.requestBody(getAddToCartJson(offerId))
				.execute();
	}

	public static void showPage(String html) {
		writeToFile("page.html", html);
		loadBrowser(new File("page.html").getAbsolutePath());
	}
	
//	private static Response jResponse = null;
//	private static String response = "";
//	private static final String BASE_URL = "https://www.walmart.com";
//	private static final String LOGIN_LINK = "<a href=\"/account/login?";
//	private static final String LOGGED_IN_TEXT = "purchase history";
//	private static final String LOGIN_PAGE_TEXT = "<input type=\"text\" id=\"email\"";
//	private static final String ACCOUNT_PAGE = BASE_URL + "/account";
//	private static final String USER_LOGIN_PAGE = BASE_URL + "/account/login?ref=domain";
//	private static final String USER_LOGIN_POST_PAGE = BASE_URL + "/account/electrode/api/signin?ref=domain";
//	private static final String USERAGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36"; 
//	private static final Integer DEFAULT_TIMEOUT = 30000;
	
//	private static final String LOGIN = "jefferybrannon@gmail.com";
//	private static final String PASSWORD = "airman404";
//	private static final String LOGIN = "starlett270@gmail.com";
//	private static final String PASSWORD = "Tayl0r07";
	
}
