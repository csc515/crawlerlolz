package edu.missouristate.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.jsoup.nodes.Document;

public class RequestResponse implements Serializable {
	
	private Document doc;
	private int statusCode;	
	private HttpEntity entity;
	private String responseBody;
	private CloseableHttpResponse response;
	private static final long serialVersionUID = 3946592307978688200L;
	
	public static final String VALUE = "value";
	public static final String TEXT = "text";
	
	//List<String> idSearchList = new ArrayList<String>();
	
	private Map<String, String> cookies;
	private Map<String, String> idSearchMap = new HashMap<String, String>();
	private Map<String, String> idSearchResults = new HashMap<String, String>();
	private Map<String, String> headerMap = new HashMap<String, String>();
	private Map<String, String> parameterMap = new HashMap<String, String>();
	private Map<String, String> hiddenElementMap = new HashMap<String, String>();
	
	// Session Variables
	private String sessionId;
	private String jSessionId;
	private String aspNetSessionId;
	
	public RequestResponse() {
	}

	/**
	 * <p>
	 * The id represent the element in the html for which you are searching. The current types that are supported 
	 * now include RequestResponseData.TEXT and RequestResponseData.VALUE.
	 * </p>
	 * @param id - Example: &lt;span id="foo" value="foo"&gt;
	 * @param type - RequestResponseData.TEXT or RequestResponseData.VALUE
	 */
	public void addIdSearch(String id, String type) {
		idSearchMap.put(id, type);
	}
	
	public void addIdSearchValue(String id, String value) {
		idSearchResults.put(id, value);
	}
	
	public void addParameter(String id, String value) {
		parameterMap.put(id, value);
	}
	
	public void addHeader(String id, String value) {
		headerMap.put(id, value);
	}
	
	public void addHiddenElement(String id, String value) {
		hiddenElementMap.put(id, value);
		parameterMap.put(id, value);
	}

	public Map<String, String> getIdSearchMap() {
		return idSearchMap;
	}

	public void setIdSearchMap(Map<String, String> idSearchMap) {
		this.idSearchMap = idSearchMap;
	}

	public Map<String, String> getIdSearchResults() {
		return idSearchResults;
	}

	public void setIdSearchResults(Map<String, String> idSearchResults) {
		this.idSearchResults = idSearchResults;
	}

	public Map<String, String> getHeaderMap() {
		return headerMap;
	}

	public void setHeaderMap(Map<String, String> headerMap) {
		this.headerMap = headerMap;
	}

	public Map<String, String> getParameterMap() {
		return parameterMap;
	}

	public void setParameterMap(Map<String, String> parameterMap) {
		this.parameterMap = parameterMap;
	}

	public Map<String, String> getHiddenElementMap() {
		return hiddenElementMap;
	}

	public void setHiddenElementMap(Map<String, String> elementMap) {
		this.hiddenElementMap = elementMap;
	}

	public HttpEntity getEntity() {
		return entity;
	}

	public void setEntity(HttpEntity entity) {
		this.entity = entity;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getResponseBody() {
		return responseBody;
	}

	public void setResponseBody(String responseBody) {
		this.responseBody = responseBody;
	}

	public CloseableHttpResponse getResponse() {
		return response;
	}

	public void setResponse(CloseableHttpResponse response) {
		this.response = response;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Document getDoc() {
		return doc;
	}

	public void setDoc(Document doc) {
		this.doc = doc;
	}
	
	public Map<String, String> getCookies() {
		return cookies;
	}

	public void setCookies(Map<String, String> cookies) {
		this.cookies = cookies;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getAspNetSessionId() {
		return aspNetSessionId;
	}

	public void setAspNetSessionId(String aspNetSessionId) {
		this.aspNetSessionId = aspNetSessionId;
	}

	public String getjSessionId() {
		return jSessionId;
	}

	public void setjSessionId(String jSessionId) {
		this.jSessionId = jSessionId;
	}
}
