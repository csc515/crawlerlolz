package edu.missouristate.util;

public class Settings {

	public static final String BASE_URL = "https://www.walmart.com";
	public static final String LOGIN_LINK = "<a href=\"/account/login?";
	public static final String LOGGED_IN_TEXT = "purchase history";
	public static final String LOGIN_PAGE_TEXT = "<input type=\"text\" id=\"email\"";
	public static final String ACCOUNT_PAGE = BASE_URL + "/account";
	public static final String CART_PAGE = BASE_URL + "/cart";
	
	public static final String PRODUCT_PS5 = "PS5";
	public static final String PRODUCT_PS5_OFFER_ID = "E4463E9F89294147BB57304778A66539";
	public static final String PS5_PAGE = BASE_URL + "/ip/PlayStation-5-Console/363472942";
	public static final String PS5_HEADER = "PlayStation 5 Console";
	
	public static final String PRODUCT_CASE = "Case";
	public static final String PRODUCT_CASE_OFFER_ID = "B139906B8DB1474B92AF3425EA4C0E8D";
	public static final String CASE_PAGE = BASE_URL + "/ip/Nintendo-Switch-Game-Traveler-Deluxe-Travel-Case-Black/840414689";
	public static final String CASE_HEADER = "Nintendo Switch Game Traveler Deluxe Travel Case, Black";
	
	public static final String PRODUCT_POGO = "Pogo";
	public static final String PRODUCT_POGO_OFFER_ID = "DD872A95499F4B278C55C2258CC73A01";
	public static final String POGO_PAGE = BASE_URL + "/ip/Flybar-Master-Pogo-Stick-for-Kids-Age-9-and-Up-80-to-160-Lbs-Toy-For-Kids-9-and-Up/24273793?selected=true";
	public static final String POGO_HEADER = "Flybar Master Pogo Stick for Kids Age 9 and Up";
	
	public static final String USER_LOGIN_PAGE = BASE_URL + "/account/login?ref=domain";
	public static final String USER_LOGIN_POST_PAGE = BASE_URL + "/account/electrode/api/signin?ref=domain";
	
	public static final String OUT_OF_STOCK_TEXT = "Get in-stock alert";
	public static final String ADD_TO_CART_TEXT = "Add to cart</span>";
	
	public static final String USERAGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36";

	public static final String JEFFERY = "Jeffery";
	public static final String JESSICA = "Jessica";
	
	public static final String JEFFERY_LOGIN = "jefferybrannon@gmail.com";
	public static final String JEFFERY_PASSWORD = "";
	public static final String JESSICA_LOGIN = "starlett270@gmail.com";
	public static final String JESSICA_PASSWORD = "";	
	
	public static final Integer DEFAULT_TIMEOUT = 30000;
	
	 
}
