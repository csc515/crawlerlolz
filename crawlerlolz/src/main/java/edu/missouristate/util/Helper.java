package edu.missouristate.util;

import java.io.ByteArrayOutputStream;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Helper {
	
    public static boolean isInteger(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

	public static String getJson(List<?> list) throws Exception {
	    final ByteArrayOutputStream out = new ByteArrayOutputStream();
	    final ObjectMapper mapper = new ObjectMapper();
	    mapper.writeValue(out, list);
	    final byte[] data = out.toByteArray();
	    return new String(data);
	}
	
	@SuppressWarnings("rawtypes")
	public static void gc() {
		try {
			Object obj = new Object();
			WeakReference ref = new WeakReference<Object>(obj);
			obj = null;
			
			while (ref.get() != null) {
				System.gc();
			}			
		} catch (Exception e) {
			e.printStackTrace();
			e.getMessage();
		}
	}
    
	public static boolean hasItems(Object[] array) {
		
		if (array != null && array.length > 0)
			return true;
		
		return false;
	}
	
	public static boolean hasItems(List<?> list) {
		
		if (list != null && list.size() > 0)
			return true;
		
		return false;
	}

	public static boolean hasItems(Map<?,?> map) {
		
		if (map != null && map.size() > 0)
			return true;
		
		return false;
	}
}
