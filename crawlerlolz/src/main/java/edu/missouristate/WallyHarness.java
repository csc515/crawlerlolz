package edu.missouristate;

import java.util.HashMap;
import java.util.Map;

import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;

import edu.missouristate.util.BotHelper;
import edu.missouristate.util.Settings;

public class WallyHarness {
	private static Response jResponse = null;
	private static String response = "";
	private static Map<String, String> cookies = new HashMap<String, String>(); 
	private static boolean outOfStock = true;
	private static boolean addToCart = false;
	private int purchaseCount = 0;
	private static Document document = null;
	private static String html = "";
	private static String productName = "";
	private static String productOfferId = "";
	private static String productPage = "";
	private static String productHeader = "";
	private static String login = "";
	private static String password = "";
	
	public static void main(String[] args) throws Exception {
		// Set Product
		setProduct(Settings.PRODUCT_PS5);
		
		// Set Credentials
		setCredentials(Settings.JEFFERY);
		
		// Visit Walkmart's Login Page
		getWalmartLoginPage();
		
		// Login into Walmart Account
		postWalmartLogin();
		
		// Go to Account Page to Make sure authentication was successful 
		getWalmartAccountPage();
		//verifyPs5IsAvailable();
		
		// Keep checking if PRODUCT is available
		assertPageContainsContentLoop(productPage, productHeader, 
				Settings.ADD_TO_CART_TEXT, Settings.DEFAULT_TIMEOUT);
		
		// Get Cart Details
		//getCartPage(false);
		
		// Add to Cart
		addProductToCart(productOfferId);
		
		// Get Cart Details
		getCartPage(true);
		
		
	}

	/**********************************
	 *  ADD PRODUCT TO CART
	 **********************************/
	private static void addProductToCart(String offerId) throws Exception {
		
		// POST URL https://www.walmart.com/api/v3/cart/:CRT/items
		jResponse = BotHelper.postAddToCart(Settings.DEFAULT_TIMEOUT, cookies, productOfferId);
		document = jResponse.parse();
		html = document.toString();
		//System.out.println(html);
		cookies = jResponse.cookies();
	}
	
	/**********************************
	 *  GET Walmart ACCOUNT Page
	 *  Verify we are logged in. Visit Account URL and check if it reads "<a href="/account/login?" 
	 *  or if it reads "purchase history"
	 **********************************/
	private static void getCartPage(boolean showPage) throws Exception {
		jResponse = BotHelper.getPage(Settings.CART_PAGE, cookies, Settings.DEFAULT_TIMEOUT);
		response = jResponse.body();
		cookies = jResponse.cookies();
		document = jResponse.parse();
		html = document.toString();
		
		if (showPage) {
			BotHelper.showPage(html);
		}
		//System.out.println(html);
//		boolean loggedIn = BotHelper.isLoggedIntoWalmart(html);
//		
//		if (!loggedIn) {
//			System.out.println("Not Logged In! Exiting...");
//			System.exit(0);
//		}
	}

	/**
	 * SET PRODUCT
	 * @param product
	 */
	private static void setProduct(String product) {
		product = (product == null) ? "" : product.toLowerCase();
		switch (product) {
		case "case":
			productName = Settings.PRODUCT_CASE;
			productOfferId = Settings.PRODUCT_CASE_OFFER_ID;
			productPage = Settings.CASE_PAGE;
			productHeader = Settings.CASE_HEADER;
			break;
		case "pogo":
			productName = Settings.PRODUCT_POGO;
			productOfferId = Settings.PRODUCT_POGO_OFFER_ID;
			productPage = Settings.POGO_PAGE;
			productHeader = Settings.POGO_HEADER;
			break;
		case "ps5":
			productName = Settings.PRODUCT_PS5;
			productOfferId = Settings.PRODUCT_PS5_OFFER_ID;
			productPage = Settings.PS5_PAGE;
			productHeader = Settings.PS5_HEADER;
			break;
		default:
			break;
		}
	}

	/**
	 * SET CREDENTIALS
	 * @param account
	 */
	private static void setCredentials(String account) {
		account = (account == null) ? "" : account.toLowerCase();
		switch (account) {
		case "jeffery":
			login = Settings.JEFFERY_LOGIN;
			password = Settings.JEFFERY_PASSWORD;
			break;
		case "jessica":
			login = Settings.JESSICA_LOGIN;
			password = Settings.JESSICA_PASSWORD;
			break;
		default:
			break;
		}
	}

	/**
	 * ASSERT PAGE CONTAINS CONTENT LOOP
	 * @param page
	 * @param header
	 * @param searchText
	 * @param timeout
	 * @throws Exception
	 */
	private static void assertPageContainsContentLoop(String page, String header, 
			String searchText, Integer timeout) throws Exception {
		
		timeout = (timeout == null) ? Settings.DEFAULT_TIMEOUT : timeout;
		while (addToCart == false) {
			//addToCart = checkPageContent(PS5_PAGE, PS5_HEADER, ADD_TO_CART_TEXT);
			addToCart = checkPageContent(page, header, searchText, cookies, timeout);

			if (addToCart == false) {
				try {
					System.out.println("'Add To Cart' button missing :(");
					Thread.sleep(1000);
				} catch (Exception e) {}
			}
		}
		System.out.println("Add To Cart is Available!!");
	}

	/**
	 * VERIFY PS5 IS AVAILABLE
	 */
	private static void verifyPs5IsAvailable() {
		/**********************************
		 *  Load PS5 Page
		 *  Verify it is on hold or available
		 **********************************/
//		while (outOfStock) {
//			outOfStock = checkOutOfStock(PS5_PAGE, OUT_OF_STOCK_TEXT);
//			//outOfStock = checkOutOfStock(CASE_PAGE, OUT_OF_STOCK_TEXT);
//			
//			if (outOfStock) {
//				try {
//					System.out.println("Out of stock! :(" );
//					Thread.sleep(1000);
//				} catch (Exception e) {}
//			}
//		}
//		
//		System.out.println("Item is in stock!");
	}

	private static void getWalmartAccountPage() throws Exception {
		/**********************************
		 *  GET Walmart ACCOUNT Page
		 *  Verify we are logged in. Visit Account URL and check if it reads "<a href="/account/login?" 
		 *  or if it reads "purchase history"
		 **********************************/
		jResponse = BotHelper.getPage(Settings.ACCOUNT_PAGE, cookies, Settings.DEFAULT_TIMEOUT);
		response = jResponse.body();
		cookies = jResponse.cookies();
		document = jResponse.parse();
		html = document.toString();
		//System.out.println(html);
		boolean loggedIn = BotHelper.isLoggedIntoWalmart(html);
		
		if (!loggedIn) {
			System.out.println("Not Logged In! Exiting...");
			System.exit(0);
		}
	}

	private static void postWalmartLogin() throws Exception {
		/**********************************
		 *  POST Walmart Login Page
		 *  Example JSON (Returned Object)
		 *   {"status":"OK","payload":{"viewData":{},"firstName":"Jeffery","lastName":"Brannon","cid":"f0454ab7-b4ef-2bb3-e044-001517f43a86","omsCustomerId":"undefined","customerId":"f0454ab7-b4ef-2bb3-e044-001517f43a86","isAssociate":false,"sodAttribute":null,"customerType":"INDIVIDUAL","ccpaReportAttribute":"false","wmPlus":{"value":"INACTIVE","ttl":86400000},"subscribed":false,"sodCookie":null,"oneAppEnabled":null}}
		 **********************************/
		jResponse = BotHelper.postWalmartLogin(Settings.DEFAULT_TIMEOUT, cookies, 
				login, password);
		document = jResponse.parse();
		html = document.toString();
		cookies = jResponse.cookies();
		
		if (!html.contains("\"status\":\"OK\"")) {
			System.out.println("Status not equal to \"OK.\" Authorization Failed? Response: " + html);
			System.exit(0);
		}
	}

	/**********************************
	 *  GET Walmart LOGIN Page
	 **********************************/
	private static void getWalmartLoginPage() throws Exception {
		jResponse = BotHelper.getPage(Settings.USER_LOGIN_PAGE, cookies, 
				Settings.DEFAULT_TIMEOUT);
		response = jResponse.body();
		cookies = jResponse.cookies();
	}
	
	public static boolean checkPageContent(String page, String header, 
			String searchText, Map<String, String> cookies, 
			Integer timeout) throws Exception {
		
		cookies = (cookies == null) ? new HashMap<String, String>() : cookies;
		timeout = (timeout == null) ? Settings.DEFAULT_TIMEOUT : timeout;
		jResponse = BotHelper.getPage(page, cookies, timeout);
		response = jResponse.body();
		cookies = jResponse.cookies();
		Document document = jResponse.parse();
		String html = document.toString();
		
		if (BotHelper.pageContains(html, header)) {
			if (BotHelper.pageContains(html, searchText)) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

//	private static boolean checkOutOfStock(String page, String outOfStockText) throws Exception {		
//		jResponse = JsoupHelper.getPage(page, cookies, DEFAULT_TIMEOUT);
//		response = jResponse.body();
//		cookies = jResponse.cookies();
//		Document document = jResponse.parse();
//		String html = document.toString();
//		
//		if (JsoupHelper.pageContains(html, PS5_HEADER)) {
//		//if (JsoupHelper.pageContains(html, CASE_HEADER)) {
//			if (JsoupHelper.pageContains(html, outOfStockText)) {
//				return true;
//			} else {
//				return false;
//			}
//		} else {
//			return true;
//		}
//	}
	
}