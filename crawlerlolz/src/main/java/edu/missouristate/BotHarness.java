package edu.missouristate;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class BotHarness {
	private static final String WWW_GOOGLE_COM = "http://www.google.com/";
	private final static String getLoginUrl = "http://localhost:8080/login"; 
	private final static String postLoginUrl = "http://localhost:8080/login";
	private Map<String, String> data = new HashMap<String, String>();
	private Map<String, String> cookies = new HashMap<String, String>();
	private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36";
	
	public BotHarness() throws Exception {
		
		// Visit the site to pick up any existing cookies that need to be managed
		Document doc = visitSite(getLoginUrl);
		printResponse(doc);
		
		// Post the information to the page
		String token = getTokenFromResponse(doc);
		data = getLoginData(token, "admin", "password");
		doc = postSite(postLoginUrl, data);
		printResponse(doc);
	}

	private Document visitSite(String url) throws Exception {				
		Response response = Jsoup.connect(url)
				.userAgent(USER_AGENT)
				.referrer(WWW_GOOGLE_COM)
	            .cookies(cookies)
	            .method(Method.GET)
	            .followRedirects(true)
	            .execute();
		
		return Jsoup.parse(response.body());
	}
	
	private Document postSite(String url, Map<String, String> data) throws Exception {
		Response response = Jsoup.connect(url)
				.userAgent(USER_AGENT)
				.referrer(WWW_GOOGLE_COM)
	            .cookies(cookies)
	            .data(data)
	            .method(Method.POST)
	            .followRedirects(true)
	            .execute();
		
		return Jsoup.parse(response.body());
	}

	private String getTokenFromResponse(Document doc) {
		String token = doc.getElementById("token").attr("value");
		return token;
	}
	
	private Map<String, String> getLoginData(String token, String username, String password) {
		Map<String, String> data = new HashMap<String, String>();
		data.put("token", token);
		data.put("username", username);
		data.put("password", password);
		return data;
	}
	
	private void printResponse(Document doc) {
		System.out.println(doc.html());		
	}
	
	public static void main(String[] args) throws Exception {
		new BotHarness();
	}
}
